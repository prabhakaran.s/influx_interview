//
//  Resource.swift
//  PrabhakaranTask
//
//  Created by Prabhakaran on 03/08/22.
//

import Foundation
struct Resource<T: Codable> {
    let url: URL
    var httpMethod: HTTPMethod = .get
    var body: Data? = nil
}

extension Resource {
    init(url: URL) {
        self.url = url
    }
}
