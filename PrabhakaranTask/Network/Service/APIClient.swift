//
//  APIClient.swift
//  PrabhakaranTask
//
//  Created by Prabhakaran on 03/08/22.
//

import Foundation

final class ApiClient {
    
    private static let jsonContentType = "application/json"
    static let sharedInstance = ApiClient()
    
    private init() {}
    
    public func sendRequest<T>(withResource resource: Resource<T>, completionHandler: @escaping (Result<T, HTTPNetworkError>) -> Void) {
        
        var urlRequest = URLRequest(url: resource.url)
        urlRequest.httpMethod = resource.httpMethod.rawValue
        urlRequest.addValue(ApiClient.jsonContentType, forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            guard let data = data, error == nil else {
                completionHandler(.failure(.domainError))
                return
            }
            
            if let result = try? JSONDecoder().decode(T.self, from: data) {
                completionHandler(.success(result))
            } else {
                completionHandler(.failure(.decodingError))
            }
        }.resume()
    }
}
