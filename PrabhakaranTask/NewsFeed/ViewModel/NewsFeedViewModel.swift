//
//  NewsFeedViewModel.swift
//  PrabhakaranTask
//
//  Created by z036053 on 04/08/22.
//

import Foundation
import UIKit

final class NewsFeedViewModel{
    
    func getAllContents(callback: @escaping ((Bool) -> Void)) {
        Appcontext.shared.articles.removeAll()
        guard let url = URL(string: Constants.Api.taskUrl) else { return }
        var resource = Resource<BaseModel>(url: url)
        resource.httpMethod = .get
        
        ApiClient.sharedInstance.sendRequest(withResource: resource) {(result) in
            switch result {
            case .success(let base):
                if let data = base.articles{
                    Appcontext.shared.articles = data
                }
                callback(true)
            case .failure(let error):
                print("Failed to get data : \(error.localizedDescription)")
            }
        
        }
    }
    
    func getLikesCount(articleLike:String, callback: @escaping ((Bool) -> Void)) {
        let likeurl = "\(Constants.Api.likesUrl)\(articleLike)"
        guard let url = URL(string: likeurl) else { return }
        var resource = Resource<LikesModel>(url: url)
        resource.httpMethod = .get
        
        ApiClient.sharedInstance.sendRequest(withResource: resource) {(result) in
            switch result {
            case .success(let likes):
                Appcontext.shared.likes = likes
                callback(true)
            case .failure(let error):
                print("Failed to get data : \(error.localizedDescription)")
            }
        
        }
    }
    
    func getCommentCount(articleComment:String, callback: @escaping ((Bool) -> Void)) {
        let likeurl = "\(Constants.Api.commentUrl)\(articleComment)"
        guard let url = URL(string: likeurl) else { return }
        var resource = Resource<CommentsModel>(url: url)
        resource.httpMethod = .get
        
        ApiClient.sharedInstance.sendRequest(withResource: resource) {(result) in
            switch result {
            case .success(let comments):
                Appcontext.shared.comments = comments
                callback(true)
            case .failure(let error):
                print("Failed to get data : \(error.localizedDescription)")
            }
        
        }
    }
    
}
