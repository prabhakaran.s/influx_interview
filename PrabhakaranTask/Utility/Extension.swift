//
//  Extension.swift
//  PrabhakaranTask
//
//  Created by Prabhakaran on 03/08/22.
//

import Foundation
import UIKit
extension UIViewController {
    public func showAlert(message: String) {
        let alert = UIAlertController(title: "Prabhakaran Task", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
        return
    }
    
    public func showAlert(message: String? = nil, completion: ((UIAlertAction) -> Void)? = nil) {
                let alert = UIAlertController(title: "Prabhakaran Task", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel,
                                      handler: completion))
        
        self.present(alert, animated: true, completion:nil)
    }
    
    //Table NIB Register
    public func tableNibRegister(view : UITableView, cell : String){
        view.register(UINib(nibName: cell, bundle: nil), forCellReuseIdentifier: cell)
    }
    
    //push controller
    public func pushVC(identifier : String){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: identifier)
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    //popOrDismiss
    public func popOrDismiss(){
        if navigationController != nil{
            navigationController?.popViewController(animated: true)
        }else{
            dismiss(animated: true, completion: nil)
        }
    }
    
    //Network
    public func isNetworkReachable() -> NSError? {
        if Reachability.isConnectedToNetwork() {
            return nil
        } else {
            let dict = NSDictionary(objects: ["Please check your internet connection"], forKeys: [NSLocalizedDescriptionKey as NSCopying])
            self.showAlert(message: "Please check your internet connection")
            return NSError(domain: "", code: 600, userInfo: dict as? [String : Any])
        }
    }
    
}
