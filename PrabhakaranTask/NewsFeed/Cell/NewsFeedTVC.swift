//
//  NewsFeedTVC.swift
//  PrabhakaranTask
//
//  Created by Prabhakaran on 14/07/22.
//

import UIKit

class NewsFeedTVC: UITableViewCell {

    @IBOutlet weak var authorLbl: UILabel!
    @IBOutlet weak var describeLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var likesLbl: UILabel!
    @IBOutlet weak var commentLbl: UILabel!
    
   var viewModel = NewsFeedViewModel()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgView.layer.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(data:Articles){
        if let url = URL(string: data.urlToImage ?? ""){
            DispatchQueue.global().async {
                if let img = try? Data(contentsOf: url){
                    DispatchQueue.main.async {
                        self.imgView.image = UIImage(data: img)
                    }
                }else{
                    self.imgView.image = UIImage(named: "placeholder")
                }
            }
        }
        authorLbl.text = data.author
        describeLbl.text = data.description
        
        let splitArticleUrl = data.url?.components(separatedBy: "://")
        let articleUrl = splitArticleUrl?[1]
        let replaceUrl = articleUrl?.replacingOccurrences(of: "/", with: "-")
        
        DispatchQueue.main.async {
            self.getLikes(likes: replaceUrl ?? "")
            self.getComments(comments: replaceUrl ?? "")
        }
        
    }
    
    func getLikes(likes : String){
        viewModel.getLikesCount(articleLike: likes){_ in
            DispatchQueue.main.async {
                self.likesLbl.text = "Likes: \(Appcontext.shared.likes?.likes ?? 0)"
            }
        }
    }
    
    func getComments(comments : String){
        viewModel.getCommentCount(articleComment: comments){_ in
            DispatchQueue.main.async {
                self.commentLbl.text = "Comments: \(Appcontext.shared.comments?.comments ?? 0)"
            }
        }
    }
    
    
}
