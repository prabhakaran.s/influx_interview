//
//  HTTPMethod.swift
//  PrabhakaranTask
//
//  Created by Prabhakaran on 03/08/22.
//

import Foundation
public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}
