//
//  NewsFeedVC.swift
//  PrabhakaranTask
//
//  Created by Prabhakaran on 03/08/22.
//

import UIKit

class NewsFeedVC: UIViewController {
    
    @IBOutlet weak var tblView: UITableView!{
        didSet{
            tblView.delegate = self
            tblView.dataSource = self
        }
    }
    
    
    var viewModel = NewsFeedViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableNibRegister(view: tblView, cell: Constants.Cell.newsFeedTVC)
        let connectivity = isNetworkReachable()
        if connectivity == nil{
            getArticles()
        }
    }
    
    private func getArticles() {
        self.viewModel.getAllContents(){_ in
            DispatchQueue.main.async {
                self.tblView.reloadData()
            }
        }
    }
}



extension NewsFeedVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Appcontext.shared.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.Cell.newsFeedTVC) as! NewsFeedTVC
        let data = Appcontext.shared.articles[indexPath.row]
        cell.configure(data: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Appcontext.shared.articles[indexPath.row].url == "" || Appcontext.shared.articles[indexPath.row].url == nil{
            showAlert(message: "No url found")
        }else{
            let vc = self.storyboard?.instantiateViewController(withIdentifier: Constants.VC.webViewVC) as! WebViewVC
            vc.articles = Appcontext.shared.articles[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
}
