//
//  Constant.swift
//  PrabhakaranTask
//
//  Created by Prabhakaran on 03/08/22.
//

import Foundation
import CoreData
import UIKit

struct Constants {
    
    static let appdelegateSharedInstance = UIApplication.shared.delegate as! AppDelegate
    
    struct Cell{
        static let newsFeedTVC   = "NewsFeedTVC"
    }
    
    struct VC{
        static let webViewVC    = "WebViewVC"
    }
    
    struct Api {
        static let taskUrl      =  "https://newsapi.org/v2/everything?q=tesla&from=2022-08-05&sortBy=publishedAt&apiKey=\(Constants.APIKey.key)"
        static let likesUrl     =   "https://cn-news-info-api.herokuapp.com/likes/"
        static let commentUrl   =   "https://cn-news-info-api.herokuapp.com/comments/"
    }
    
    struct APIKey{
        static let key  =   "eab664655ee34f04beab9b8a8bf75a40"
    }
}

class Appcontext{
    static let shared = Appcontext()
    var articles = [Articles]()
    var likes : LikesModel?
    var comments : CommentsModel?
}
