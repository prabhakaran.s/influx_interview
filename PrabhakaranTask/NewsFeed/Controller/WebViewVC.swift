//
//  WebViewVC.swift
//  PrabhakaranTask
//
//  Created by Prabhakaran on 03/08/22.
//

import UIKit
import WebKit


class WebViewVC: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    var articles : Articles?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let url = URL (string: articles?.url ?? ""){
            let requestObj = URLRequest(url: url)
            webView.load(requestObj)
        }
        
    }
    

   

}
