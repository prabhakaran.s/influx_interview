//
//  HTTPNetworkError.swift
//  PrabhakaranTask
//
//  Created by Prabhakaran on 03/08/22.
//

import Foundation
public enum HTTPNetworkError: Error {
    case domainError
    case decodingError
}
